<?php

namespace App\Controller;

use App\Queries\EntryQueries;
use App\Queries\HabitQueries;
use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\{Request, Response, JsonResponse};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Carbon\Carbon;

class DashController extends AbstractController
{
    /**
     * Index page
     */
    public function index(Connection $db): Response
    {
        // Get habits
        $habits = HabitQueries::getUserUnarchivedHabits(
            $db,
            $this->getUser()->getId()
        );

        return $this->redirect('/app/' . $habits[0]['id']);
    }

    /**
     * Single habit page
     */
    public function single(Request $request, Connection $db, $habitId): Response
    {
        // Get habits
        $habits = HabitQueries::getUserUnarchivedHabits(
            $db,
            $this->getUser()->getId()
        );

        // Active habit
        $activeHabit = $habits[array_search(
            $habitId,
            array_column($habits, 'id')
        )];

        // Get month
        $month = $request->query->get('m') ?? date('Y-m-d');
        $month = $month <= date('Y-m-d') ? $month : date('Y-m-d');

        $prevMonth = (new Carbon($month))->startOfMonth()->subMonth()->toDateString();
        $nextMonth = (new Carbon($month))->startOfMonth()->addMonth()->toDateString();
        $monthName = (new Carbon($month))->format('F Y');
        $start = (new Carbon($month))->firstOfMonth()->startOfWeek(Carbon::SUNDAY);
        $end = (new Carbon($month))->lastOfMonth()->endOfWeek(Carbon::SATURDAY);

        // Get start date and end date
        $days = [];
        $startDate = $start->format('Y-m-d');
        $endDate = $end->format('Y-m-d');

        // Get list of entries for this date interval
        $unstructuredEntries = EntryQueries::getEntriesForHabitInInterval($db, $habitId, $startDate, $endDate);

        // Structure entries
        $entries = [];
        foreach ($unstructuredEntries as $entry) {
            $key = (new Carbon($entry['registed_at']))->format('Y-m-d');
            $entries[$key] = true;
        }

        // Get current month
        $currentMonth = (new Carbon($month))->format('m');

        // Fill with the rest of the days
        while ($startDate != $endDate) {
            $startDate = $start->format('Y-m-d');
            $days[] = [
                'date' => $startDate,
                'isCurrentMonth' => $start->format('m') === $currentMonth,
                'cantSet' => $startDate > $month || $startDate < $activeHabit['start_at'],
                'isSet' => isset($entries[$startDate]) && $entries[$startDate],
                'hasFailed' => !isset($entries[$startDate]) && $startDate <= $month && $startDate >= $activeHabit['start_at']
            ];
            $start->add(1, 'days');
        }

        return $this->render('dash.html.twig', [
            'habits' => $habits,
            'activeHabit' => $activeHabit,
            'prevMonth' => $prevMonth,
            'nextMonth' => $nextMonth,
            'monthName' => $monthName,
            'days' => $days
        ]);
    }

    /**
     * Save day entry from frontend
     */
    public function saveDay(Connection $db, Request $request, int $habitId): JsonResponse
    {
        // Get date selected from request
        $date = $request->get('date');

        // Insert or remove into database
        $action = EntryQueries::addOrDelete($db, $date, $habitId);

        // Return response with true or false
        return $this->json([
            'action' => $action
        ]);
    }
}
