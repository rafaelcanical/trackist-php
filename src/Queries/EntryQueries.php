<?php

namespace App\Queries;

use Doctrine\DBAL\Connection;

class EntryQueries
{
    /**
     * Get all entries of a specific habit in a given time interval
     */
    static public function getEntriesForHabitInInterval(Connection $db, int $habitId, string $start, string $end)
    {
        $query = <<<SQL
            SELECT *
            FROM entry
            WHERE habit_id = :habit_id
                AND (registed_at >= :start AND registed_at <= :end)
        SQL;

        $statement = $db->prepare($query);
        $statement->bindValue('habit_id', $habitId);
        $statement->bindValue('start', $start);
        $statement->bindValue('end', $end);
        $resultSet = $statement->executeQuery();
        $results = $resultSet->fetchAllAssociative();

        return $results;
    }

    /**
     * Get all entries of a specific habit in a given time interval
     */
    static public function addOrDelete(Connection $db, string $date, int $habitId)
    {
        // Check if entry already exists
        $query = <<<SQL
            SELECT *
            FROM entry
            WHERE habit_id = :habit_id
                AND registed_at = :date
        SQL;

        $statement = $db->prepare($query);
        $statement->bindValue('habit_id', $habitId);
        $statement->bindValue('date', $date);
        $resultSet = $statement->executeQuery();
        $result = $resultSet->fetchOne();

        $action = '';

        if ($result) {
            // Delete existing entry
            $action = 'deleted';
            $query = <<<SQL
                DELETE FROM entry
                WHERE habit_id = :habit_id
                    AND registed_at = :date
            SQL;

            $statement = $db->prepare($query);
            $statement->bindValue('habit_id', $habitId);
            $statement->bindValue('date', $date);
            $statement->executeQuery();
        } else {
            // Insert new entry
            $action = 'added';
            $query = <<<SQL
                INSERT INTO entry (habit_id, registed_at)
                VALUES (:habit_id, :date)
            SQL;

            $statement = $db->prepare($query);
            $statement->bindValue('habit_id', $habitId);
            $statement->bindValue('date', $date);
            $statement->executeQuery();
        }

        return $action;
    }
}
