<?php

namespace App\Queries;

use Doctrine\DBAL\Connection;

class HabitQueries
{
    /**
     * Get unarchived habits of a specific user
     */
    static public function getUserUnarchivedHabits(Connection $db, int $userId)
    {
        $query = <<<SQL
            SELECT *
            FROM habit
            WHERE user_id = :user_id
                AND is_archived = 0
        SQL;

        $statement = $db->prepare($query);
        $statement->bindValue('user_id', $userId);
        $resultSet = $statement->executeQuery();
        $results = $resultSet->fetchAllAssociative();

        return $results;
    }
}
