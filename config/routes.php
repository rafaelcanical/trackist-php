<?php

use App\Controller\DashController;
use App\Controller\PublicController;
use App\Controller\RegistrationController;
use App\Controller\SecurityController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    // Landing page
    $routes->add('landing', '/')->controller([PublicController::class, 'landing'])->methods(['GET']);

    // Authentication routes
    $routes->add('app_register', '/register')->controller([RegistrationController::class, 'register'])->methods(['GET', 'POST']);
    $routes->add('app_login', '/sign-in')->controller([SecurityController::class, 'login'])->methods(['GET', 'POST']);
    $routes->add('app_logout', '/sign-out')->controller([SecurityController::class, 'logout'])->methods(['GET', 'POST']);

    // Dashboard routes
    $routes->add('app_dash', '/app')->controller([DashController::class, 'index'])->methods(['GET']);
    $routes->add('app_single_habit', '/app/{habitId}')->controller([DashController::class, 'single'])->methods(['GET']);
    $routes->add('app_save_day_entry', '/app/{habitId}/save')->controller([DashController::class, 'saveDay'])->methods(['POST']);
};
